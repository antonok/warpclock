package com.antonok.warpclock

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import androidx.datastore.core.DataStore
import androidx.datastore.dataStore
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking

/**
 * Implementation of App Widget functionality.
 */
class WarpclockWidget : AppWidgetProvider() {
    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    companion object {
        internal fun updateAppWidget(
            context: Context, appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {
            val warpclockSettings = runBlocking { context.settingsStore.data.first() }
            val warpAmount = warpclockSettings.warpAmount
            val hours = warpAmount / 60
            val minutes = warpAmount % 60

            val widgetText = if (minutes == 0) {
                context.getString(R.string.appwidget_text).format(hours)
            } else {
                context.getString(R.string.appwidget_text_minutes).format(hours, minutes)
            }
            // Construct the RemoteViews object
            val views = RemoteViews(context.packageName, R.layout.warpclock_widget)
            views.setTextViewText(R.id.appwidget_text, widgetText)

            views.setOnClickPendingIntent(R.id.appwidget_text,
                getPendingIntent(context))

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }

        private fun getPendingIntent(context: Context): PendingIntent {
            val intent = Intent(context, WidgetActionReceiver::class.java).apply {
                action = ACTION_SET_WARP_ALARM
            }

            // Create a PendingIntent to broadcast the actual intent to the correct destination
            return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }

    }
}

