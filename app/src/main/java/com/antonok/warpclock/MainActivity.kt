package com.antonok.warpclock

import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.datastore.core.DataStore
import androidx.datastore.dataStore
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking

const val WARP_AMOUNT_MIN = 1
const val WARP_AMOUNT_MAX = 24 * 60

const val HOUR_INCREMENT = 60
const val LARGE_MINUTE_INCREMENT = 10

const val DATASTORE_FILE = "warpclock_settings"
val Context.settingsStore: DataStore<WarpclockSettings> by dataStore(DATASTORE_FILE, serializer = WarpclockSettingsSerializer)

class MainActivity : AppCompatActivity() {
    private lateinit var hoursTextView: TextView
    private lateinit var incrementHoursButton: Button
    private lateinit var decrementHoursButton: Button

    private lateinit var minutesTextView: TextView
    private lateinit var increment10MinutesButton: Button
    private lateinit var increment1MinuteButton: Button
    private lateinit var decrement10MinutesButton: Button
    private lateinit var decrement1MinuteButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        hoursTextView = findViewById<TextView>(R.id.textViewHours);
        incrementHoursButton = findViewById<Button>(R.id.incrementHoursButton);
        decrementHoursButton = findViewById<Button>(R.id.decrementHoursButton);

        minutesTextView = findViewById<TextView>(R.id.textViewMinutes);
        increment10MinutesButton = findViewById<Button>(R.id.increment10MinutesButton);
        increment1MinuteButton = findViewById<Button>(R.id.increment1MinuteButton);
        decrement10MinutesButton = findViewById<Button>(R.id.decrement10MinutesButton);
        decrement1MinuteButton = findViewById<Button>(R.id.decrement1MinuteButton);

        val warpclockSettings = runBlocking { settingsStore.data.first() }
        updateViews(warpclockSettings.warpAmount)
    }

    fun setAlarm(@Suppress("UNUSED_PARAMETER") view: View) {
        val mIntent = Intent(this, AlarmIntentService::class.java).apply {
            action = ACTION_SET_WARP_ALARM
        }
        AlarmIntentService().enqueueWork(this, mIntent)
    }

    fun incrementHours(@Suppress("UNUSED_PARAMETER") view: View) {
        runBlocking { modifyWarpAmount(HOUR_INCREMENT) }
    }

    fun decrementHours(@Suppress("UNUSED_PARAMETER") view: View) {
        runBlocking { modifyWarpAmount(-HOUR_INCREMENT) }
    }

    fun increment10Minutes(@Suppress("UNUSED_PARAMETER") view: View) {
        runBlocking { modifyWarpAmount(LARGE_MINUTE_INCREMENT) }
    }

    fun increment1Minute(@Suppress("UNUSED_PARAMETER") view: View) {
        runBlocking { modifyWarpAmount(1) }
    }

    fun decrement10Minutes(@Suppress("UNUSED_PARAMETER") view: View) {
        runBlocking { modifyWarpAmount(-LARGE_MINUTE_INCREMENT) }
    }

    fun decrement1Minute(@Suppress("UNUSED_PARAMETER") view: View) {
        runBlocking { modifyWarpAmount(-1) }
    }

    private suspend fun modifyWarpAmount(offset: Int) {
        var newWarpAmount = 0
        settingsStore.updateData { d ->
            newWarpAmount = (d.warpAmount + offset).coerceAtLeast(WARP_AMOUNT_MIN).coerceAtMost(WARP_AMOUNT_MAX)
            d.toBuilder().setWarpAmount(newWarpAmount).build()
        }
        updateViews(newWarpAmount)
    }

    private fun updateViews(warpAmount: Int) {
        val hours = warpAmount / 60
        val minutes = warpAmount % 60

        val faintSpan = ForegroundColorSpan(ContextCompat.getColor(application, R.color.faintText))
        val ssbHours = SpannableStringBuilder("%d".format(hours))
        ssbHours.append("h").setSpan(faintSpan, ssbHours.length - "h".length, ssbHours.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        val ssbMinutes = SpannableStringBuilder("%d".format(minutes))
        ssbMinutes.append("m").setSpan(faintSpan, ssbMinutes.length - "m".length, ssbMinutes.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        hoursTextView.text = ssbHours
        minutesTextView.text = ssbMinutes

        val canIncrement = warpAmount < WARP_AMOUNT_MAX
        incrementHoursButton.isEnabled = canIncrement
        increment10MinutesButton.isEnabled = canIncrement
        increment1MinuteButton.isEnabled = canIncrement

        val canDecrement = warpAmount > WARP_AMOUNT_MIN
        decrementHoursButton.isEnabled = canDecrement
        decrement10MinutesButton.isEnabled = canDecrement
        decrement1MinuteButton.isEnabled = canDecrement

        val widgetIds = AppWidgetManager.getInstance(application).getAppWidgetIds(ComponentName(application, WarpclockWidget::class.java))
        val intent = Intent(this, WarpclockWidget::class.java).apply {
            action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIds)
        }
        sendBroadcast(intent)
    }
}