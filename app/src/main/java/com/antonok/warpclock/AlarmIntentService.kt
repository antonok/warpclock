package com.antonok.warpclock

import android.content.Intent
import android.content.Context
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.os.Handler
import android.os.Looper
import android.provider.AlarmClock
import android.widget.Toast
import androidx.core.app.JobIntentService
import androidx.datastore.core.DataStore
import androidx.datastore.dataStore
import java.time.Instant
import java.time.LocalDate
import java.util.*
import java.util.Calendar.HOUR_OF_DAY
import java.util.Calendar.MINUTE
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking

const val ACTION_SET_WARP_ALARM = "com.antonok.warpclock.action.SET_WARP_ALARM"

/**
 * An [IntentService] subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
class AlarmIntentService : JobIntentService() {
    private var mHandler: Handler = Handler(Looper.getMainLooper())

    fun enqueueWork(context: Context, intent: Intent) {
        enqueueWork(context, AlarmIntentService::class.java, 2, intent)
    }

    override fun onCreate() {
        super.onCreate()
    }

    override fun onHandleWork(intent: Intent) {
        when (intent.action) {
            ACTION_SET_WARP_ALARM -> {
                handleActionSetAlarm()
            }
        }
    }

    /**
     * Handle action SetAlarm in the provided background thread with the provided
     * parameters.
     */
    private fun handleActionSetAlarm() {
        val currentTime = Calendar.getInstance()

        val settings = runBlocking { settingsStore.data.first() }

        val warpAmount = settings.warpAmount

        val hour = currentTime.get(HOUR_OF_DAY)
        val minute = currentTime.get(MINUTE)

        val alarmMinute = (minute + warpAmount) % 60
        val alarmHour = (hour + (minute + warpAmount) / 60) % 24

        // Set the new alarm using an Intent
        val alarmIntent = Intent(AlarmClock.ACTION_SET_ALARM).apply {
            putExtra(AlarmClock.EXTRA_HOUR, alarmHour)
            putExtra(AlarmClock.EXTRA_MINUTES, alarmMinute)
            putExtra(AlarmClock.EXTRA_SKIP_UI, true)
        }
        alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(alarmIntent)

        val alarmTime = if(alarmMinute < 10) {
            "$alarmHour:0$alarmMinute"
        } else {
            "$alarmHour:$alarmMinute"
        }

        val hours = warpAmount / 60
        val minutes = warpAmount % 60

        val toastMsg = "Warp ahead ${createWarpAheadDescription(hours, minutes)} to $alarmTime"

        // Show a toast.
        mHandler.post {
            Toast.makeText(this, toastMsg, Toast.LENGTH_SHORT).show()
        }

        //Update the widget
        val appWidgetManager = AppWidgetManager.getInstance(this)
        val widgetIds = appWidgetManager.getAppWidgetIds(ComponentName(this, WarpclockWidget::class.java))
        for (appWidgetId in widgetIds) {
            WarpclockWidget.updateAppWidget(applicationContext, appWidgetManager, appWidgetId)
        }
    }

    companion object {
        /**
         * Starts this service to set an alarm with the given parameters. If
         * the service is already performing a task this action will be queued.
         *
         * @see IntentService
         */
        @JvmStatic
        fun startSetAlarm(context: Context) {
            val intent = Intent(context, AlarmIntentService::class.java).apply {
                action = ACTION_SET_WARP_ALARM
            }
            context.startService(intent)
        }
    }
}

fun createWarpAheadDescription(hours: Int, minutes: Int) : String {
    val sb = StringBuilder()
    if (hours > 0) {
        sb.append("$hours hour")
        if (hours != 1) {
            sb.append("s")
        }
    }
    if (hours > 0 && minutes > 0) {
        sb.append(" and ")
    }
    if (minutes > 0) {
        sb.append("$minutes minute")
        if (minutes != 1) {
            sb.append("s")
        }
    }

    return sb.toString()
}