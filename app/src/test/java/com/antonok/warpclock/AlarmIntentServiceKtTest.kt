package com.antonok.warpclock

import org.junit.Assert.*
import org.junit.Test

class AlarmIntentServiceKtTest {
    @Test
    fun testCreateWarpAheadDescription() {
        assertEquals("1 minute", createWarpAheadDescription(0, 1))
        assertEquals("2 minutes", createWarpAheadDescription(0, 2))
        assertEquals("1 hour", createWarpAheadDescription(1, 0))
        assertEquals("1 hour and 1 minute", createWarpAheadDescription(1, 1))
        assertEquals("1 hour and 2 minutes", createWarpAheadDescription(1, 2))
        assertEquals("2 hours", createWarpAheadDescription(2, 0))
        assertEquals("2 hours and 1 minute", createWarpAheadDescription(2, 1))
        assertEquals("2 hours and 2 minutes", createWarpAheadDescription(2, 2))
        assertEquals("10 hours", createWarpAheadDescription(10, 0))
        assertEquals("10 hours and 1 minute", createWarpAheadDescription(10, 1))
        assertEquals("10 hours and 2 minutes", createWarpAheadDescription(10, 2))
    }
}